# Installation prerequisites.

REQUIRED_DEBIAN_PACKAGES="apache2-mpm-event libapache2-mod-fcgid php5-cgi php5-gd php5-sqlite sqlite3 curl"

install_pkg() {
  DEBIAN_FRONTEND=noninteractive \
  apt-get install -q -y --force-yes -o 'DPkg::Options::=--force-confdef' $@
}

install_sources() {
  if [ ! -e /etc/apt/sources.list.d/owncloud.list ]; then
    log "installing Owncloud apt source"
    wget -O- http://download.opensuse.org/repositories/isv:ownCloud:community/Debian_6.0/Release.key \
      | apt-key add -
    echo "deb http://download.opensuse.org/repositories/isv:ownCloud:community/Debian_6.0/ /" \
      > /etc/apt/sources.list.d/owncloud.list
    apt-get -qq update
  fi
}

install_packages() {
  test ! -x /usr/bin/apt-get && fatal "Not on a Debian system!"
  log "installing required Debian packages"
  for pkg in $REQUIRED_DEBIAN_PACKAGES ; do
    dpkg-query --status ${pkg} | grep -q 'Status: install' >/dev/null 2>&1 || install_pkg ${pkg}
  done
}

#install_users() {
#  if ! getent passwd ${USER} >/dev/null; then
#    log "creating user '${USER}'"
#    adduser --system --home /home/${USER} --shell /bin/bash \
#        --disabled-password ${USER}
#  fi
#  if ! getent group ${SHARED_GROUP} >/dev/null; then
#    log "creating group '${SHARED_GROUP}'"
#    addgroup --system ${SHARED_GROUP}
#    adduser ${USER} ${SHARED_GROUP}
#    adduser www-data ${SHARED_GROUP}
#  fi
#}

fix_owncloud_datadir() {
  if [ "${DATADIR}" != "/var/www/owncloud/data" ]; then
    test -d ${DATADIR} || mkdir ${DATADIR}
    chown www-data:www-data ${DATADIR}
    chmod 0770 ${DATADIR}
    if [ ! -L /var/www/owncloud/data ]; then
      rm -fr /var/www/owncloud/data
      ln -s ${DATADIR} /var/www/owncloud/data
    fi
  fi
}

install_owncloud() {
  install_pkg owncloud

  fix_owncloud_datadir

  # Setup Apache for Owncloud.
  for mod in rewrite deflate headers ssl fcgid; do
    test -e /etc/apache2/mods-enabled/${mod}.load || a2enmod ${mod}
  done
  for mod in status; do
    test -e /etc/apache2/mods-enabled/${mod}.load && a2dismod ${mod}
  done

  local onionsite
  case "${ENABLE_ANONYMOUS_BROWSING}" in
  [yY]*) onionsite=onion.index ;;
  *) onionsite=onion.noindex ;;
  esac
  rm -f /etc/apache2/sites-enabled/onion 2>/dev/null
  ln -s /etc/apache2/sites-available/${onionsite} /etc/apache2/sites-enabled/onion

  # Create the HS document root and put a little welcome message in it.
  mkdir -p /var/www/owncloud/data/admin/files
  echo '<h4>Welcome to OnionBox</h4>' > /var/www/owncloud/data/admin/files/README.html

  /usr/sbin/apache2ctl restart

  if [ ! -e /var/www/owncloud/data/owncloud.db ]; then
    local pass=$(dd if=/dev/urandom bs=1 count=8 2>/dev/null | md5sum | cut -c-12)
    echo -n "${pass}" > /root/.owncloud.p
    chmod 0400 /root/.owncloud.p
    log "initialize Owncloud database"
    sleep 1
    curl -F adminlogin=admin \
	-F adminpass=${pass} \
	-F directory=${DATADIR} \
	-F install=true \
	-F dbtype=sqlite \
	-k https://localhost:443/index.php
    if [ $? -gt 0 ]; then
      fatal "Owncloud database setup failed!"
    fi
  fi

  # Disable a bunch of unused fancy owncloud apps.
  log "setting up Owncloud apps"
  (for app in files_odfviewer files_imageviewer files_videoviewer \
	files_sharing files_pdfviewer files_texteditor calendar contacts \
	user_migrate admin_migrate media gallery ; do
    echo "UPDATE oc_appconfig SET configvalue='no' WHERE configkey='enabled' AND appid='${app}';"
  done) | sqlite3 /var/www/owncloud/data/owncloud.db
  chown www-data:www-data /var/www/owncloud/data/owncloud.db

  OWNCLOUD_ADMIN_PASSWORD="$(cat /root/.owncloud.p)"
}

install_tor_source() {
  if [ ! -e /etc/apt/sources.list.d/torproject.list ]; then
    log "installing Tor apt source"
    echo "deb http://deb.torproject.org/torproject.org squeeze main" \
      > /etc/apt/sources.list.d/torproject.list
    apt-get -qq update
    install_pkg deb.torproject.org-keyring
    apt-get -qq update
  fi
}

install_tor() {
  # We need to install Tor first, and ensure that it starts with our
  # configuration, so that the hidden service URL is generated and
  # is available for later substitutions.
  test -d /etc/tor || mkdir -p /etc/tor
  install -m 644 -o root -g root ${src_dir}/etc/tor/torrc \
      /etc/tor/torrc
  install_pkg tor

  # Wait for the hidden service address to appear.
  while true ; do
    test -f /var/lib/tor/hidden_service/hostname && break
    log "waiting for Tor to initialize the Hidden Service..."
    sleep 5
  done
}

get_tor_hidden_service_address() {
  cat /var/lib/tor/hidden_service/hostname
}

