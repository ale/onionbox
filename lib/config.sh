# Configuration helpers.


# Configuration defaults go here.

# Name of this host (used to reach the server on the public Internet).
HOSTNAME=

# Location of the top-level data directory.
DATADIR=/var/www/owncloud/data

# Whether to enable indexes on the Tor HS.
ENABLE_ANONYMOUS_BROWSING=YES


# List of mandatory configuration variables.
MANDATORY_CONFIG_VARS="DATADIR HOSTNAME ENABLE_ANONYMOUS_BROWSING"

config_read() {
    local file="$1"
    test ! -f "${file}" && fatal "Config file ${file} does not exist!"
    . "${file}"

    # Autodetect hostname.
    if [ -z "${HOSTNAME}" ]; then
      HOSTNAME=$(hostname -f)
    fi

    # Validation.
    case ${HOSTNAME} in
    *.*) ;;
    *) fatal "Set HOSTNAME to a fully qualified domain name!";;
    esac

    missing=
    for var in ${MANDATORY_CONFIG_VARS} ; do
        val=`eval echo "\\\$${var}"`
        test -z "${val}" && missing="${missing}${missing:+ }${var}"
    done
    test -n "${missing}" && fatal "Configuration variables ${missing} are missing!"
}

config_dump() {
    cat <<EOF

Configuration Summary
---------------------

HOSTNAME=$HOSTNAME
DATADIR=$DATADIR

EOF
}

