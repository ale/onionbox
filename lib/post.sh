
final_message() {

  cat <<EOF


******************************
OnionBox installation complete
******************************

To use your new OnionBox:

1) Copy /etc/onionbox/onionbox.pem to your computer and install it
   as a trusted SSL certificate.

2) Visit your personal Owncloud installation at:

    https://${HOSTNAME}/

   use the following credentials to login:

    Username: admin
    Password: ${OWNCLOUD_ADMIN_PASSWORD}

3) Share files from your public folder at:

    http://${ONION_DOMAIN}/

4) Enjoy!


EOF
}

postinstall() {
  install_packages
  install_owncloud
  final_message
}

