# Logging.

logMsg() {
    echo "$*" 1>&2
}

debug() {
    logMsg " .  *$"
}

log() {
    logMsg "[*] $*"
}

error() {
    logMsg "[!] $*"
}

fatal() {
    error "$*"
    exit 1
}
