# Replace @VAR@ definitions in stdin with variables taken from the
# environment.
patsubst() {
  local vars="$@"
  local sed_cmdline=""
  for var in ${vars} ; do
    val=$(eval "echo \$${var}")
    sed_cmdline="-e s,@${var}@,${val},g ${sed_cmdline}"
  done
  sed ${sed_cmdline}
}

# Call patsubst() on a tree of files.
patsubst_tree() {
  local src="$1"
  local dst="$2"
  shift 2
  local export_vars="$@"

  find ${src} -type f -print \
  | (while read file ; do
      file_dst=$(echo ${file} | sed -e "s,^${src},${dst},")
      dir_dst=$(dirname "${file_dst}")
      test -d "${dir_dst}" || mkdir -p "${dir_dst}"
      debug "${file} -> ${file_dst}"
      patsubst ${export_vars} \
	  < "${file}" \
	  > "${file_dst}"
      done)
}

# Copy the entire hierarchy with the necessary substitutions.
copy_fs() {
  local etc_src="${src_dir}/etc"
  local etc_dst="/etc"

  ONION_DOMAIN=$(get_tor_hidden_service_address)
  patsubst_tree ${etc_src} ${etc_dst} HOSTNAME DATADIR ONION_DOMAIN

  # Fix perms post-copy.
  chmod 0755 /etc/init.d/php-cgi
}

