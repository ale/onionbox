
generate_ssl_cert() {
  local cert_dir=/etc/onionbox
  local cert_file=${cert_dir}/onionbox.pem
  local key_file=${cert_dir}/onionbox.key

  test -f ${cert_file} -a -f ${key_file} && return
  test -d ${cert_dir} || mkdir -p ${cert_dir}

  log "creating self-signed SSL certificate for '${HOSTNAME}'"

  local subj="
C=XX
ST=XX
O=OnionBox
localityName=Somewhere
commonName=${HOSTNAME}
organizationalUnitName=
emailAddress=admin@${HOSTNAME}
"

  openssl req -batch -subj "$(echo -n "$subj" | tr '\n' '/')" \
     -x509 -extensions v3_req -newkey rsa:2048 -nodes -keyout ${key_file} -out ${cert_file}

  chown root:root ${cert_file} ${key_file}
  chmod 0644 ${cert_file}
  chmod 0400 ${key_file}
}

