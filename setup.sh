#!/bin/bash

if [ `whoami` != root ]; then
  echo "Run with 'sudo'!" 1>&2
  exit 1
fi

# Autodetect root dir.
src_dir=$(dirname "$0")
src_dir=$(cd "${src_dir:-.}" && pwd)

if [ ! -f "${src_dir}/config" ]; then
  cat 1>&2 <<EOF
Create a configuration file first! Start with:

    \$ cp config.dist config
    \$ vi config

and run this command again.
EOF
  exit 1
fi

# Import our function library.
for src in "${src_dir}/lib/"*.sh ; do
  . "${src}"
done


config_read ${src_dir}/config
config_dump
preinstall
copy_fs
postinstall

exit 0
