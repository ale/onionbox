
OnionBox
========

Share your files anonymously via the Tor network.

It's just a script that will set up a system (for instance, a VM dedicated
to this purpose) with Owncloud on the public IP and a simple HTTP server
on a Tor Hidden Service.  Some effort has been spent into making sure that
the hidden HTTP server does not leak information about the public IP, but
please do not try to leak life-or-death documents with this!  There are
far more serious ways to do that.  The objective of this software is to
facilitate casual sharing via the Tor Hidden Service network.


Installation
------------

Step 1: Get a cheap VM somewhere and install a bare-bones, minimal Debian
stable. Encrypt it if you're paranoid. OnionBox might work with other Debian
versions and perhaps Ubuntu, but such setups are currently untested.

Step 2: Install OnionBox::

    $ git clone http://git.autistici.org/ale/onionbox.git
    $ cd onionbox
    $ cp config.dist config
    $ vi config
    $ sudo ./setup.sh

Step 3: ... profit? No, really, you should probably get an Owncloud client
from http://owncloud.org/ and use it to synchronize the files you want to
share, then using the public Tor2Web network to share links to those files
on your Hidden Service.


